@ECHO OFF

SET CC=gcc
SET CFLAGS=-std=c99 -Wall -Wextra -Wpedantic -O3
SET SRC=src/start.c src/parser.c src/editor.c
SET EXE=txtodo

IF EXIST %EXE% del %EXE%
%CC% %CFLAGS% %SRC% -o %EXE%
