#!/bin/sh

CC="gcc"
CFLAGS="-std=c99 -Wall -Wextra -Wpedantic"
if [ "$1" = "-r" ]; then
	CFLAGS="${CFLAGS} -O3"
else
	CFLAGS="${CFLAGS} -O0 -ggdb -fsanitize=address"
fi
SRC="src/start.c src/parser.c src/editor.c"
EXE="txtodo"

set -xe

rm -f ${EXE}
rm -f tmp_*
exec ${CC} ${CFLAGS} ${SRC} -o ${EXE}
