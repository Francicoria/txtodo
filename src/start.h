#include <stdio.h>
#include <time.h>
#if defined(_WIN32) && !defined(WINDOWS_PLATFORM)
#define WINDOWS_PLATFORM
#include <windows.h>
#endif

void enable_colors(void);

#define MAX_TOKENS 128
#define MAX_TASKS 128

#define clear_screen() do { printf("\x1b[;H\x1b[J"); } while (0)
#define here(s) do { printf("%s:%d: " s "\n", __FILE__, __LINE__); } while (0)

double timeID(void);
void recieved_eof(void);
