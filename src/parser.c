#include <stdio.h>
#include <stdbool.h>
#include "start.h"
#include "parser.h"
#include "editor.h"

str make_str(char *src) {
	size_t len = 0;
	while (src[len] != '\0') { ++len; }
	str result = {
		.content = src,
		.len = len,
		.cap = 2 * len,
	};
	return result;
}

// @todo Here we are increasing content pointer, which is incredibly wrong because
// we need the original pointer given by us from malloc/realloc to eventually free it.
// We could add an additional item to `str` struct to hold the original pointer, and then
// content would point to it as a "viewer" or maybe something else, but this seems the
// easier option right now.
void chop_left(str *s, size_t n) {
	if (s->len < n) chop_left(s, s->len);
	s->content += n;
	s->len -= n;
}
void chop_right(str *s, size_t n) {
	if (s->len < n) chop_right(s, s->len);
	s->len -= n;
	s->content[s->len] = 0;
}
void chop_left_spaces(str *s) {
	while (s->len > 0 && s->content[0] == ' ') chop_left(s, 1);
}
void chop_right_spaces(str *s) {
	while (s->len > 0 && s->content[s->len-1] == ' ') chop_right(s, 1);
}
void append_char(str *s, char c) {
	if (s->cap <= s->len) {
		if (s->cap == 0) s->cap = 2;
		s->cap *= 2;
		s->content = realloc(s->content, s->cap);
	}
	s->content[s->len] = c;
	s->len += 1;
}
bool starts_with(str s, str pattern) {
	if (pattern.len == 0) return true;
	if (s.len < pattern.len) return false;
	for (size_t i = 0; i < pattern.len; ++i) {
		if (pattern.content[i] == '*') continue;
		if (pattern.content[i] != s.content[i]) return false;
	}
	return true;
}

Token tokenize_word(str *s) {
	Token result = { .type = TYPE_WORD };
	size_t len = 0;
	// @todo finish this code, which should compute the lenght and then copy the string
	// from s->content to result.word with `memcpy` or something else.
	while (s->len > 0 && s->content[len] != ' ' && s->content[len] != '\n') { ++len; }
	//if (*(s->content) == '\n') return result;
	append_char(&result.word, *(s->content));
	chop_left(s, len);

	here("UNFINISHED");
	exit(1);

	return result;
}
Token next_token(str *s) {
	Token result = {0};

	chop_left_spaces(s);
	if (s->len == 0) {
		result.type = TYPE_END;
		return result;
	}

	//puts("1");
	// @todo implement task dates
	// exaple: "2022-05-11 My birthday"
	// format is YYYY-MM-DD
	switch (*(s->content)) {
		case '\n':
			chop_left(s, 1);
			result.type = TYPE_NEWLINE;
			return result;
		case 'x':
			// x <WORDS>
			//if (s->len < 2) return tokenize_word(s);
			if (!starts_with(*s, make_str("x "))) return tokenize_word(s);
			chop_left(s, 1);

			result.type = TYPE_FINISH_MARK;
			return result;
		case '(':
			if (!starts_with(*s, make_str("(*) "))) return tokenize_word(s);
			char letter = *(s->content + 1);
			if (letter < 'A' || letter > 'Z') return tokenize_word(s);
			chop_left(s, 3);

			result.priority_letter = letter;
			result.type = TYPE_PRIORITY;
			return result;
		case '@':
			if (!starts_with(*s, make_str("@*"))) return tokenize_word(s);
			chop_left(s, 1);
			result = tokenize_word(s);
			result.type = TYPE_CONTEXT;
			return result;
		case '+':
			if (!starts_with(*s, make_str("+*"))) return tokenize_word(s);
			chop_left(s, 1);
			result = tokenize_word(s);
			result.type = TYPE_PROJECT;
			return result;
		default: return tokenize_word(s);
	}
	here("unreachable");
}

Task parse_task(Tokens ts, size_t *cursor) {
#define TASK_BODY_SIZE 32
#define TASK_CTXS_SIZE 16
#define TASK_PRJS_SIZE 16
	Task result = {
		.body_cap = TASK_BODY_SIZE,
		.body = malloc(sizeof(str) * result.body_cap),
		.ctxs_cap = TASK_CTXS_SIZE,
		.ctxs = malloc(sizeof(str) * result.ctxs_cap),
		.prjs_cap = TASK_PRJS_SIZE,
		.prjs = malloc(sizeof(str) * result.prjs_cap),
	};
	for (; *cursor < ts.count; ++(*cursor)) {
		Token t = ts.ts[*cursor];
		switch (t.type) {
			case TYPE_END:
				result.end = true;
				return result;
			case TYPE_NEWLINE:
				*cursor += 1;
				return result;
			case TYPE_FINISH_MARK:
				result.finished = true;
				break;
			case TYPE_PRIORITY:
				result.priority = t.priority_letter;
				break;
			case TYPE_WORD:
				if (result.body_cap <= result.body_size) {
					if (result.body_cap == 0) result.body_cap = 2;
					while (result.body_cap <= result.body_size) result.body_cap *= 2;
					result.body = realloc(result.body, sizeof(str) * result.body_cap);
					if (result.body == NULL) {
						here("realloc returned NULL, wow");
						exit(1);
					}
				}
				result.body[result.body_size] = t.word;
				result.body_size += 1;
				break;
			case TYPE_CONTEXT:
				if (result.ctxs_cap < result.ctxs_size) {
					if (result.ctxs_cap == 0) result.ctxs_cap = 2;
					while (result.ctxs_cap <= result.ctxs_size) result.ctxs_cap *= 2;
					result.ctxs = realloc(result.ctxs, sizeof(str) * result.ctxs_cap);
					if (result.ctxs == NULL) {
						here("realloc returned NULL, wow");
						exit(1);
					}
				}
				result.ctxs[result.ctxs_size] = t.word;
				result.ctxs_size += 1;
				break;
			case TYPE_PROJECT:
				if (result.prjs_cap < result.prjs_size) {
					if (result.prjs_cap == 0) result.prjs_cap = 2;
					while (result.prjs_cap <= result.prjs_size) result.prjs_cap *= 2;
					result.prjs = realloc(result.prjs, sizeof(str) * result.prjs_cap);
					if (result.prjs == NULL) {
						here("realloc returned NULL, wow");
						exit(1);
					}
				}
				result.prjs[result.prjs_size] = t.word;
				result.prjs_size += 1;
				break;
			default:
				here("A token type which has not been handled has been encountered");
				printf("type: %d\n", t.type);
				exit(1);
				break;
		}
	}
	here("No TYPE_NEWLINE or TYPE_END encountered");
	exit(1);
}

void tokenize_str(str source, Tokens *dst) {
	//printf("source = \"%s\"\n", source.content);
	//printf("lenght = %ld\n\n",  source.len);

	dst->count = 0;
	while (true) {
		if (dst->count >= MAX_TOKENS-1) {
			fprintf(stderr, "Reached %d tokens. Is this intentional? Exiting.\n", MAX_TOKENS);
			exit(1);
		}
		dst->ts[dst->count] = next_token(&source);
		//printf("  content = \"%s\"\n", task.content);
		//printf("  lenght = %ld\n", task.len);
		//printf("%3ld: ", dst->count);
		//debug_token(dst->ts[dst->count]);
		if (dst->ts[dst->count].type == TYPE_END) break;
		dst->count += 1;
	}
	dst->count += 1;
}
