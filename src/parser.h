#include <stdio.h>
#include <stdbool.h>

typedef struct {
	size_t len;
	size_t cap;
	char *content;
} str;
str make_str(char *src);
void chop_left(str *s, size_t n);
void chop_right(str *s, size_t n);
void chop_left_spaces(str *s);
void chop_right_spaces(str *s);
void append_char(str *s, char c);
bool starts_with(str s, str pattern);

typedef enum {
	TYPE_PRIORITY = 0,
	TYPE_FINISH_MARK,
	TYPE_WORD,
	TYPE_CONTEXT,
	TYPE_PROJECT,
	TYPE_NEWLINE,
	TYPE_END,
} TokenType;

typedef struct {
	TokenType type;
	char priority_letter;
	str word;
} Token;
Token next_token(str *s);

typedef struct {
	Token *ts;
	size_t count;
} Tokens;
void tokenize_str(str source, Tokens *dst);

typedef struct {
	bool end;
	bool finished;
	char priority;

	size_t body_size;
	size_t body_cap;
	str *body;

	size_t ctxs_size;
	size_t ctxs_cap;
	str *ctxs;

	size_t prjs_size;
	size_t prjs_cap;
	str *prjs;
} Task;
Task parse_task(Tokens ts, size_t *cursor);
