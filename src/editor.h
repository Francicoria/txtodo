#include <stdio.h>
#include <stdlib.h>

typedef enum {
	EDIT_NOTHING = 0,
	NEW_TASK,
	DELETE_TASK,
	REPLACE_TASK,
} EditMode_Command;

typedef enum {
	VIEW_NOTHING = 0,
	DOWN,
	UP,
	EDIT,
	QUIT,
} ViewMode_Command;

void save(FILE * file, char * filename, Task *tasks);
void print_preview(Task *tasks, int selectedTask);
void get_prompt(str *buffer);
void delete_task(Task *tasks, int index);
void create_task(Task *tasks, int index, Task task);
void edit_mode(str prompt, Task *tasks, int selectedTask);
void view_mode(Task *tasks);
