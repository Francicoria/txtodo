#include <stdio.h>
#include <stdlib.h>
#include "start.h"
#include "parser.h"
#include "editor.h"
#include "colors.h"

void save(FILE * f, char * file_path, Task *tasks) {
	if ((f = freopen(file_path, "w", f)) == NULL) {
		perror("ERROR");
		exit(1);
	}
	for (int i = 0; i < MAX_TASKS; ++i) {
		Task task = tasks[i];
		if (task.end) break;
		if (task.finished) fprintf(f, "x ");
		if (task.priority != 0) fprintf(f, "(%c) ", task.priority);
		for (size_t i = 0; i < task.body_size; ++i) {
			fprintf(f, "%s", task.body[i].content);
			if (i + 1 != task.body_size) fputc(' ', f);
		}
		if (task.ctxs_size > 0) fputc(' ', f);
		for (size_t i = 0; i < task.ctxs_size; ++i) {
			fprintf(f, "@%s", task.ctxs[i].content);
			if (i + 1 != task.ctxs_size) fputc(' ', f);
		}
		if (task.prjs_size > 0) fputc(' ', f);
		for (size_t i = 0; i < task.prjs_size; ++i) {
			fprintf(f, "+%s", task.prjs[i].content);
			if (i + 1 != task.prjs_size) fputc(' ', f);
		}
		fprintf(f, "\n");
	}
}

void print_preview(Task *tasks, int selected_task) {
	clear_screen();
	int i = 0;
	// Don't customize the colors directly in here, instead do it in colors.h
	for (; i < MAX_TASKS; ++i) {
		Task task = tasks[i];
		if (task.end) break;
		if (i == selected_task) printf("  "SELECTED"%3d"RESET"   ", i);
		else                    printf("  %3d   ", i);
		if (task.finished) printf(FINISHED"x"RESET" ");
		else               printf("  ");
		if (task.priority != 0) printf("("PRIORITY"%c"RESET") ", task.priority);
		if (task.finished) printf(STRIKETHROUGH);
		for (size_t j = 0; j < task.body_size; ++j) {
			printf("%s ", task.body[j].content);
		}
		printf(RESET);
		for (size_t j = 0; j < task.ctxs_size; ++j) {
			if (task.finished) printf(STRIKETHROUGH);
			printf(CONTEXTS"@%s"RESET" ", task.ctxs[j].content);
		}
		for (size_t j = 0; j < task.prjs_size; ++j) {
			if (task.finished) printf(STRIKETHROUGH);
			printf(PROJECTS"+%s"RESET" ", task.prjs[j].content);
		}
		printf("\n");
	}
	//printf("\x1b[%dA", i+1);
}

void delete_task(Task *tasks, int index) {
	for (; index < MAX_TASKS; ++index) {
		tasks[index] = tasks[index+1];
		if (tasks[index].end) break;
	}
}

void create_task(Task *tasks, int index, Task task) {
	Task under = tasks[index];
	tasks[index] = task;
	if (tasks[index].end) return;
	create_task(tasks, index + 1, under);
}

void get_prompt(str *buffer) {
	char c;
	for (size_t i = 0; i < buffer->cap; ++i) {
		c = fgetc(stdin);
		if (feof(stdin)) recieved_eof();
		if (c == '\n') break;
		append_char(buffer, c);
	}

	chop_left_spaces(buffer);
	chop_right_spaces(buffer);
}

void edit_mode(str prompt, Task *tasks, int selected_task) {
	EditMode_Command com = EDIT_NOTHING;
	printf("\x1b[1A\x1b[K");
	fprintf(stdout, "-"EDIT_PROMPT"Edit mode"RESET"-> ");
	get_prompt(&prompt);
	switch (prompt.content[0]) {
		case 'n':
			if (prompt.len >= 3) {
				com = NEW_TASK;
				chop_left(&prompt, 2);
			}
			break;
		case 'd':
			com = DELETE_TASK;
			break;
		case 'r':
			if (prompt.len >= 3) {
				com = REPLACE_TASK;
				chop_left(&prompt, 2);
			}
			break;
		default: break;
	}
	Token ts_buf[MAX_TOKENS] = {0};
	Tokens ts = { .ts = ts_buf };
	size_t cursor = 0;
	Task t = {0};
	switch (com) {
		case NEW_TASK:
			tokenize_str(prompt, &ts);
			cursor = 0;
			t = parse_task(ts, &cursor);
			t.end = false;
			create_task(tasks, selected_task, t);
			break;
		case DELETE_TASK:
			delete_task(tasks, selected_task);
			break;
		case REPLACE_TASK:
			delete_task(tasks, selected_task);
			tokenize_str(prompt, &ts);
			cursor = 0;
			t = parse_task(ts, &cursor);
			t.end = false;
			create_task(tasks, selected_task, t);
			break;
		default: break;
	}
}

void view_mode(Task *tasks) {
	size_t selected_task = 0;
#define MAX_PROMPT_SIZE 256
	str prompt = {
		.cap = MAX_PROMPT_SIZE,
		.content = malloc(prompt.cap),
	};
	ViewMode_Command com = VIEW_NOTHING;

	do {
		switch (com) {
			case UP:
				selected_task -= (selected_task > 0);
				break;
			case DOWN:
				if (selected_task < MAX_TASKS-1 && !tasks[selected_task + 1].end) selected_task++;
				break;
			case EDIT:
				edit_mode(prompt, tasks, selected_task);
				break;
			default: break;
		}
		while (selected_task > 0 && tasks[selected_task].end) selected_task--;

		print_preview(tasks, selected_task);

		fprintf(stdout, "-"VIEW_PROMPT"View mode"RESET"-> ");
		get_prompt(&prompt);
		com = VIEW_NOTHING;
		if (prompt.len < 2) {
			// @todo add 's' key to save with the (optionally given) filename
			// -View mode-> s `my-file.todo.txt`
			// -View mode-> s
			switch (prompt.content[0]) {
				case 'j':
					com = DOWN;
					break;
				case 'k':
					com = UP;
					break;
				case 'q':
					com = QUIT;
					break;
				case 'e':
					com = EDIT;
					break;
				default: break;
			}
		}
		prompt.len = 0;
	} while (com != QUIT);

	free(prompt.content);
}
