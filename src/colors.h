#ifndef COLORS_H_
#define COLORS_H_

#define RESET         "\x1b[m"
#define BOLD          "\x1b[1m"
#define ITALIC        "\x1b[3m"
#define UNDERLINE     "\x1b[4m"
#define SWAP_FG_BG    "\x1b[7m"
#define STRIKETHROUGH "\x1b[9m"
#define RED    "\x1b[38;5;1m"
#define GREEN  "\x1b[38;5;2m"
#define YELLOW "\x1b[38;5;3m"
#define BLUE   "\x1b[38;5;4m"
#define PURPLE "\x1b[38;5;5m"
#define CYAN   "\x1b[38;5;6m"
#define GRAY   "\x1b[38;5;7m"
#define BLACK  "\x1b[38;5;8m"

#define VIEW_PROMPT
#define EDIT_PROMPT UNDERLINE
#define SELECTED SWAP_FG_BG
#define FINISHED GRAY
#define PRIORITY BOLD RED
#define CONTEXTS YELLOW
#define PROJECTS BLUE

#endif // COLORS_H_
