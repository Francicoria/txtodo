#include <stdio.h>
#include <assert.h>
#include <time.h>
#include "start.h"
#include "parser.h"
#include "editor.h"

void enable_colors(void) {
	// enables colors on Windows.
	#ifdef WINDOWS_PLATFORM
	HANDLE winConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD winConsoleMode = 0;
	GetConsoleMode(winConsole, &winConsoleMode);
	SetConsoleMode(winConsole, winConsoleMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
	#endif
}

double timeID(void) {
	struct tm the2000 = {
		.tm_sec = 0,  .tm_min = 0, .tm_hour = 0,
		.tm_mday = 0, .tm_mon = 0, .tm_year = 120,
		.tm_yday = 0
		};
	time_t now = time(NULL);
	return difftime(now, mktime(&the2000));
}

void recieved_eof(void) {
	fprintf(stderr, "\nRecieved eof signal, exiting.\n");
	exit(1);
}

void file_to_str(FILE *f, str *dst) {
	char c;
	while ((c = fgetc(f)) != EOF) {
		append_char(dst, c);
	}
}

void debug_task(Task task) {
	printf("[%c] ", " x"[task.finished]);
	if (task.priority != 0) printf("(%c) ", task.priority);
	for (size_t i = 0; i < task.body_size; ++i) printf("%s ", task.body[i].content);
	for (size_t i = 0; i < task.ctxs_size; ++i) printf("@%s ", task.ctxs[i].content);
	for (size_t i = 0; i < task.prjs_size; ++i) printf("+%s ", task.prjs[i].content);
	puts("");
}

void debug_token(Token tok) {
	printf("<type %d> ", tok.type);
	switch(tok.type) {
		case TYPE_WORD:
			printf("word=\"%s\"", tok.word.content);
			break;
		case TYPE_CONTEXT:
			printf("context=\"%s\"", tok.word.content);
			break;
		case TYPE_PROJECT:
			printf("project=\"%s\"", tok.word.content);
			break;
		case TYPE_PRIORITY:
			printf("priority=%c", tok.priority_letter);
			break;
		case TYPE_NEWLINE:
			printf("\\n");
			break;
		default: break;
	}
	puts("");
}

// I don't anyone would unironically reach 1024 tasks in a single text file,
// but if that happens they can just increase this number.
#define TOO_MANY_TASKS 1024
void cleanup_tasks(Task *tasks) {
	for (size_t i = 0;; ++i) {
		if (i >= TOO_MANY_TASKS) {
			here("An incredible amount of tasks as been reached, are we sure this isn't a bug?");
			exit(1);
		}
		free(tasks[i].body);
		free(tasks[i].ctxs);
		free(tasks[i].prjs);
		if (tasks[i].end) break;
	}
}

int main(int argc, char **argv) {
	FILE *f;
	char *file_path = calloc(1024, 1);

	if (argc > 2) {
		fprintf(stderr, "Only one file can be passed as an argument.\nIf there are spaces in the filename, put apostrophes around it.\n");
		return 1;
	}

	// Enables colors for platforms that don't
	// support them by default (i.e. Windows)
	enable_colors();

#define MAX_TASKS 128
	Task tasks[MAX_TASKS] = {0};

	if (argc == 1) {
		double filenameTmpID = timeID();
		snprintf(file_path, 90, "tmp_%.f.todo.txt", filenameTmpID);
		if ((f = fopen(file_path, "w+")) == NULL) {
			perror("ERROR");
			return 1;
		}
		tasks[1].end = true;
	} else { // argc must be 2
		free(file_path);
		file_path = argv[1];
		if ((f = fopen(file_path, "r+")) == NULL) {
			perror("ERROR");
			return 1;
		}

		str source = {0};
#define STR_BUFFER_SIZE 1024
		char b[STR_BUFFER_SIZE] = {0};
		source.content = b;
		source.cap = STR_BUFFER_SIZE;
		file_to_str(f, &source);

		Token ts_buf[MAX_TOKENS] = {0};
		Tokens ts = {
			.ts = ts_buf,
		};
		tokenize_str(source, &ts);
		//Tokens ts = tokenize_str(source);

		size_t cursor = 0;
		for (int i = 0;; ++i) {
			if (i >= MAX_TASKS-1) {
				fprintf(stderr, "Reached %d tasks. Is this intentional? Exiting.\n", MAX_TASKS);
				exit(1);
			}
			tasks[i] = parse_task(ts, &cursor);
			if (tasks[i].end) break;
			//debug_task(tasks[i]);
		}
	}
	view_mode(tasks);
	save(f, file_path, tasks);
	fclose(f);
	if (argc == 1) free(file_path);
	cleanup_tasks(tasks);

	return 0;
}
