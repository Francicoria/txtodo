### Todo application following [todo.txt](http://todotxt.org) design

![Screenshot](media/screenshot.png)

Build on linux (GCC)
```console
$ ./build.sh
$ ./build.sh -r  # release build
```
Build on windows (MSYS2 MinGW64)
```console
$ ./build.cmd
```

Run
```console
$ ./txtodo                   # To start an empty buffer
$ ./txtodo my-file.todo.txt  # To open already existing todo.txt
```

Todo:
- [x] Tokenizer & parser
- [x] Tui editor
- [ ] QoL improvements
	- [ ] add, remove 'finished' status to tasks.
	- [ ] add, remove projects from tasks.
	- [ ] add, remove contexts from tasks.
